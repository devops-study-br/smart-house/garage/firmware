# 🦿 Prototype

Para o desenvolvimento da aplicação o seguinte setup foi utilizado:

![](../files/prototype/prototype_bb.png)

> Conversor FTDI é utilizado com o jumper em 3v3
>
> Quando o firmware for carregado, remova o fio branco que conecta IO0 ao GND

## 🛠️ Componentes utilizados

### Adaptador ESP8266

<img src="../files/esp8266%20adapter/951_2_H.png" width=150>

[Datasheet](../files/esp8266%20adapter/ESP826_Adapter_v1.0.pdf)

### Conversor ftdi ft232rl USB/serial

<img src="../files/fdti/4MD16-1.jpg" width=150>

[Datasheet](../files/fdti/Datasheet-FT232R.pdf)