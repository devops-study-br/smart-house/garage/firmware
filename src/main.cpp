#include <Arduino.h>

void setup() {
  Serial.begin(9600);
}

void loop() {
  Serial.println("Testing...");
  digitalWrite(LED_BUILTIN, HIGH);
  delay(1000);
  Serial.println("Hello Electronics");
  digitalWrite(LED_BUILTIN, LOW);
  delay(1000);
}